﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    private Rigidbody2D rigidBody;

    private Animator myAnimator;


    [SerializeField]
    private float moveSpeed;

    private bool faceRight;

    private bool attack;

    private bool slide;

	void Start () {
        faceRight = true;
        rigidBody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
	}

    void Update()
    {
        handelInput();
    }


	void FixedUpdate () {
        float horizontal = Input.GetAxis("Horizontal");

        ControlMovement(horizontal);

        Flip(horizontal);

        handelAttacks();

        resetValues();
	}

    private void ControlMovement (float horizontal)
    {
        if (!myAnimator.GetBool("slide") && !this.myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("attack"))
        {
            rigidBody.velocity = new Vector2(horizontal * moveSpeed, rigidBody.velocity.y);
        }

        if(slide && !this.myAnimator.GetCurrentAnimatorStateInfo(0).IsName("Slide"))
        {
            myAnimator.SetBool("slide", true);
        }
        else if(!this.myAnimator.GetCurrentAnimatorStateInfo(0).IsName("Slide"))
        {
            myAnimator.SetBool("slide", false);
        }


        myAnimator.SetFloat("Speed", Mathf.Abs(horizontal));
    }

    private void handelAttacks()
    {
        if (attack && !this.myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("attack"))
        {
            myAnimator.SetTrigger("Attack");
            rigidBody.velocity = Vector2.zero;
        }

    }

    private void handelInput()
    {
        if(Input.GetKeyDown(KeyCode.C))
        {
            attack = true;
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            slide = true;
        }
    }


    private void Flip(float horizontal)
    {
        if (horizontal > 0 && !faceRight || horizontal < 0 && faceRight)
        {
            faceRight = !faceRight;

            Vector3 theScale = transform.localScale;

            theScale.x *= -1;
            transform.localScale = theScale;
        }


    }

    private void resetValues()
    {
        attack = false;

        slide = false;
    }

}
